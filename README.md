# Bolex

An HTML5 video player designed for reviewing animation. 

## Features:
* __Keyframes:__ set keys in the timeline and flip through them 
* __Frame by Frame:__ step through the video by individual frames
* More?